from collections import defaultdict
output = []

def process_child(id, nodes, level, input_json):
    output.append((next(item for item in input_json if item["id"] == id)))
    for child in sorted(nodes.get(id, [])):
        process_child(child, nodes, level + 1, input_json)

def sort_categories_for_insert(input_json):
    nodes, roots = defaultdict(set), set()

    for item in input_json:
        if item["parent_id"] == None:
            roots.add(item["id"])
        else:
            nodes[item["parent_id"]].add(item["id"])

    for id in sorted(roots):
        print (id, nodes)
        process_child(id, nodes, 0, input_json)

    return output

# input_json = [
#   {
#     "name": "Accessories",
#     "id": 1,
#     "parent_id": 20,
#   },
#   {
#     "name": "Watches",
#     "id": 57,
#     "parent_id": 1
#   },
#   {
#     "name": "Men",
#     "id": 20,
#     "parent_id": None
#   }
# ]


# print (sort_categories_for_insert(input_json))

